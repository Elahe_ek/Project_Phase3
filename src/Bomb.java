import javax.swing.*;
import java.io.File;
import java.io.Serializable;

import static java.lang.Thread.sleep;

public class Bomb implements Serializable{
    ImageIcon image;
    int i;
    int j;
    boolean jPlusContinue = true;
    boolean jMinusContinue = true;
    boolean iPlusContinue= true;
    boolean iMinusContinue= true;

    Bomb(int i, int j) {
        this.i = i;
        this.j = j;
        this.image = new ImageIcon(new File("").getAbsolutePath()+"/images/b.png");
    }

    public void explode(BomberMan bomberMan) {

        Thread exploding = new Thread(() -> {

            try {

                Thread.sleep(4800);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            if(bomberMan.isAlive)
                this.image = new ImageIcon(new File("").getAbsolutePath()+"/images/explosion.png");
        });
        exploding.start();

    }

    public void quickExplode(BomberMan bomberMan) {

        Thread nowExploding = new Thread(() -> {

            if(bomberMan.isAlive)
                this.image = new ImageIcon(new File("").getAbsolutePath()+"/images/explosion.png");
        });
        nowExploding.start();

    }

    public void quickRemove(RectanglesPanel r , BomberMan b ) {

        Thread qRemovingBomb = new Thread(() -> {

            try {
                sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            ((Grass) r.rectangles[this.i][this.j]).bomb = null;
            r.rectangles[this.i][this.j].canCross = true;
            r.rectangles[this.i][this.j].canCrossBomberman = true;
            r.bombs.remove(this);
            destroyNeighbors(b , r);
        });

        qRemovingBomb.start();
    }

    public void remove(RectanglesPanel r , BomberMan b ) {

        Thread removingBomb = new Thread(() -> {

            try {
                sleep(5000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            ((Grass) r.rectangles[this.i][this.j]).bomb = null;
            r.rectangles[this.i][this.j].canCross = true;
            r.rectangles[this.i][this.j].canCrossBomberman =  true;
            r.bombs.remove(this);
            destroyNeighbors(b , r);

        });

        removingBomb.start();
    }

    public void destroyNeighbors(BomberMan b ,RectanglesPanel rP) {

        for (int k = 0; k <rP.gameFrame.players.size() ; k++) {
            if (this.i == rP.gameFrame.players.get(k).location_i && this.j == rP.gameFrame.players.get(k).location_j) {
                rP.gameFrame.players.get(k).die();
                if(!rP.gameFrame.players.get(k).equals(b))
                    b.score+=100;
            }
        }

        for (int i = 1; i <= b.bombRadius; i++) {

                int thisI = i;
                Thread exploding = new Thread(() -> {

                    try {
                        sleep(thisI * 200);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }

                    try {

                        if (this.jPlusContinue && this.j + thisI < rP.h ) {
                            if (rP.rectangles[this.i][this.j + thisI] instanceof Wall) {
                                ((Wall) rP.rectangles[this.i][this.j + thisI]).destroy();
                                rP.removeWall(b, this.i, this.j + thisI);
                                this.jPlusContinue = false;
                            }

                            for (int j = 0; j < rP.enemies.size(); j++) {
                                if (rP.enemies.get(j) != null) {
                                    if (rP.enemies.get(j).location_i == this.i && rP.enemies.get(j).location_j == this.j + thisI) {
                                        rP.enemies.get(j).die();
                                        rP.removeEnemy(j, b);
                                    }
                                }
                            }

                            if (rP.rectangles[this.i][this.j + thisI] instanceof Rock)
                                this.jPlusContinue = false;


                            for (int k = 0; k < rP.gameFrame.players.size(); k++) {
                                if (this.i == rP.gameFrame.players.get(k).location_i && this.j + thisI == rP.gameFrame.players.get(k).location_j) {
                                    rP.gameFrame.players.get(k).die();
                                    if(!rP.gameFrame.players.get(k).equals(b))
                                        b.score+=100;
                                }
                            }
                        }

                        if (this.jMinusContinue && this.j - thisI >=0) {
                            if (rP.rectangles[this.i][this.j - thisI] instanceof Wall) {
                                ((Wall) rP.rectangles[this.i][this.j - thisI]).destroy();
                                rP.removeWall(b , this.i, this.j - thisI);
                                this.jMinusContinue = false;
                            }

                            for (int j = 0; j < rP.enemies.size(); j++) {
                                if (rP.enemies.get(j) != null) {
                                    if (rP.enemies.get(j).location_i == this.i && rP.enemies.get(j).location_j == this.j - thisI) {
                                        rP.enemies.get(j).die();
                                        rP.removeEnemy(j , b);
                                    }
                                }
                            }

                            if (rP.rectangles[this.i][this.j - thisI] instanceof Rock)
                                this.jMinusContinue = false;

                            for (int k = 0; k <rP.gameFrame.players.size() ; k++) {
                                if (this.i == rP.gameFrame.players.get(k).location_i && this.j - thisI == rP.gameFrame.players.get(k).location_j) {
                                    rP.gameFrame.players.get(k).die();
                                    if(!rP.gameFrame.players.get(k).equals(b))
                                        b.score+=100;
                                }
                            }

                        }

                        if (this.iPlusContinue && this.i +thisI < rP.w) {
                            if (rP.rectangles[this.i + thisI][this.j] instanceof Wall) {
                                ((Wall) rP.rectangles[this.i + thisI][this.j]).destroy();
                                rP.removeWall(b ,this.i + thisI, this.j);
                                this.iPlusContinue = false;
                            }

                            for (int j = 0; j < rP.enemies.size(); j++) {
                                if (rP.enemies.get(j) != null) {
                                    if ( rP.enemies.get(j).location_i == this.i + thisI && rP.enemies.get(j).location_j == this.j) {
                                        rP.enemies.get(j).die();
                                        rP.removeEnemy(j, b);
                                    }
                                }

                            }

                            if (rP.rectangles[this.i + thisI][this.j] instanceof Rock)
                                this.iPlusContinue = false;

                            for (int k = 0; k <rP.gameFrame.players.size() ; k++) {
                                if (this.i +thisI == rP.gameFrame.players.get(k).location_i && this.j == rP.gameFrame.players.get(k).location_j) {
                                    rP.gameFrame.players.get(k).die();
                                    if(!rP.gameFrame.players.get(k).equals(b))
                                        b.score+=100;
                                }
                            }

                        }

                        if (this.iMinusContinue && this.i - thisI >=0) {
                            if (rP.rectangles[this.i - thisI][this.j] instanceof Wall) {
                                ((Wall) rP.rectangles[this.i - thisI][this.j]).destroy();
                                rP.removeWall(b ,this.i - thisI, this.j);
                                this.iMinusContinue = false;
                            }
                            for (int j = 0; j < rP.enemies.size(); j++) {
                                if (rP.enemies.get(j) != null) {
                                    if (rP.enemies.get(j).location_i == this.i - thisI && rP.enemies.get(j).location_j == this.j) {
                                        rP.enemies.get(j).die();
                                        rP.removeEnemy(j, b);
                                    }
                                }
                            }
                            if (rP.rectangles[this.i - thisI][this.j] instanceof Rock)
                                this.iMinusContinue = false;

                            for (int k = 0; k <rP.gameFrame.players.size() ; k++) {
                                if (this.i - thisI== rP.gameFrame.players.get(k).location_i && this.j == rP.gameFrame.players.get(k).location_j) {
                                    rP.gameFrame.players.get(k).die();
                                    if(!rP.gameFrame.players.get(k).equals(b))
                                        b.score+=100;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                });
                exploding.start();

        }


    }
}
