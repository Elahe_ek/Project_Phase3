import javax.swing.*;
import java.io.File;
import java.io.Serializable;

public class Enemy3 extends Enemy implements Serializable{

    public Enemy3(int location_i, int location_j) {
        super(location_i, location_j , 3);
        speed = 2;
        this.image = new ImageIcon(new File("").getAbsolutePath() + "/images/monster-3.png");
    }

    @Override
    void enemyMove(RectanglesPanel r) {
        chasingMove(r);
    }
}
