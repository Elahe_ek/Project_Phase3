import javax.swing.*;
import java.io.File;
import java.io.Serializable;

class Rectangle extends java.awt.Rectangle implements Serializable{

    ImageIcon grassImage ;
    ImageIcon image;
    boolean canCross;
    boolean canCrossBomberman;

    Rectangle(int x, int y, int width, int height ,boolean cross, ImageIcon image1) {
        super(x, y, width, height);
        grassImage = new ImageIcon(new File("").getAbsolutePath()+"/images/unknown.jpg") ;
        image = image1;
        canCross = cross;
    }

    Rectangle(int x, int y, int width, int height ,boolean cross) {
        super(x, y, width, height);
        grassImage = new ImageIcon(new File("").getAbsolutePath()+"/images/unknown.jpg") ;
        canCross = cross;
        canCrossBomberman = cross;
    }


}
