import javax.swing.*;
import java.io.File;
import java.io.Serializable;

public class ClientBomberMan implements Serializable{
    int bombLimit ;
    int bombRadius;
    ImageIcon image;
    int location_i ;
    int location_j;
    boolean isAlive;
    int pixelI;
    int pixelJ;
    int ghadam;
    int score;
    int speed ;
    long zero ;
    boolean ghostMode ;
    boolean bombControling;

    public ClientBomberMan() {
        bombLimit = 1;
        bombRadius = 2;
        location_i = 0;
        location_j = 0;
        pixelI =5;
        pixelJ =5;
        speed = 2 ;
        ghadam = speed*5 ;
        isAlive = true;
        score =0 ;
        ghostMode = false;
        bombControling = false;
        image = new ImageIcon(new File("").getAbsolutePath()+"/images/superhero.png");
        zero = System.currentTimeMillis();

    }

    public void die() {
        this.image = new ImageIcon(new File("").getAbsolutePath()+"/images/dead.png");
        isAlive= false;
        JOptionPane.showMessageDialog(null, "Game Over");
        System.out.println("died");
    }
}
