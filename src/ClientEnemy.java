import javax.swing.*;
import java.io.Serializable;

public class ClientEnemy implements Serializable{
    ImageIcon image;
    int pixelI;
    int pixelJ;

    public ClientEnemy(ImageIcon image, int pixelI, int pixelJ) {
        this.image = image;
        this.pixelI = pixelI;
        this.pixelJ = pixelJ;
    }
}
