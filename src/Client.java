// // A Java program for a Client

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class Client {
    // initialize socket and input output streams
    private Socket socket            = null;
    // for IO
    ObjectOutputStream os ;
    ObjectInputStream is ;

    ClientFrame clientFrame ;
    String username , server;
    int port ;
    Menu menu;

    // if it is a game's player
    ClientGameFrame game ;

    // if it is a game's watcher
    JFrame watcher ;
    ClientGamePanel watcherGame;

    // constructor to put some information
    public Client(String server ,int port, String username, ClientFrame clientFrame) {
        this.clientFrame = clientFrame;
        this.username = username;
        this.server = server;
        this.port = port;
    }

    public boolean start() {
        // try to connect to the server
        try {
            socket = new Socket(server, port);
        }
        // if it failed not much I can so
        catch(Exception ec) {
            System.out.println("Error connectiong to server:" + ec);
            return false;
        }

        /* Creating both Data Stream */
        try
        {
            is  = new ObjectInputStream(socket.getInputStream());
            os = new ObjectOutputStream(socket.getOutputStream());
        }
        catch (IOException eIO) {
            System.out.println("Exception creating new Input/output Streams: " + eIO);
            return false;
        }
        // to send username to server
        try {
            os.writeObject(username);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // to make the menu and show it to client
        Menu();

        // success we inform the caller that it worked
        return true;
    }

    // to make the menu to start the game
    public void Menu(){
        menu = new Menu();
        menu.setSize(600 , 450);
        menu.setVisible(true);
        // new game button action listener
        menu.start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    os.writeObject(menu);
                    os.writeObject("start");
                    getGame.start();
                    menu.setVisible(false);
                    requestAction.start();

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        // game lists button action listener
        menu.list.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    os.writeObject(menu);
                    os.writeObject("list");
                    // read names of available games on the server
                    ArrayList<String> s = (ArrayList<String>) is.readObject();
                    // add game buttons to the panel below the frame
                    for (int i = 0; i < s.size() ; i++) {
                        JButton jb = new JButton(s.get(i));
                        menu.listPanel.add(jb);
                        menu.gameButtons.add(jb);
                    }
                    menu.repaint();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }

                for (int i = 0; i < menu.gameButtons.size(); i++) {
                    int number = i ;
                    // add action listener to each game button
                    menu.gameButtons.get(i).addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            // if choosed , write number of this game and the player/watcher mode selected to server
                            try {
                                os.writeObject(number);
                                os.writeObject(menu.player.isSelected());
                                if(menu.player.isSelected()) {
                                    // read if the request is available
                                    Boolean b = (Boolean) is.readObject();
                                    if (b) {
                                        // start getting the game
                                        getGame.start();
                                        menu.setVisible(false);
                                        requestAction.start();
                                    } else {
                                        // if not , disable the button to inform the client
                                        menu.gameButtons.get(number).setEnabled(false);
                                    }
                                }
                                else
                                    getGameWatcher.start();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            } catch (ClassNotFoundException e1) {
                                e1.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }
    // Thread to get game as watcher
    Thread getGameWatcher = new Thread(()->{

        watcher = new JFrame(username +" watch frame");

        // to get needed info to make a new client panel for watcher at first
        try {
            int w = (Integer)is.readObject();
            int h = (Integer)is.readObject();
            Rectangle[][] rectangles = (Rectangle[][]) is.readObject();
            ArrayList<ClientEnemy> enemies = (ArrayList<ClientEnemy>) is.readObject();
            nextLevelDoor thisDoor = (nextLevelDoor) is.readObject();
            watcherGame = new ClientGamePanel(w , h , rectangles , enemies , thisDoor);
            watcher.setSize(w*50 , h*50 );
            watcher.add(watcherGame);
            watcher.setVisible(true);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // to get game data from server
        while(!socket.isClosed()&& !watcherGame.gameOver){
            try{
                watcherGame.rectangles = (Rectangle[][]) is.readObject();
                watcherGame.enemies = (ArrayList<ClientEnemy>)is.readObject();
                watcherGame.players = (ArrayList<BomberMan>)is.readObject();
                watcherGame.thisDoor = (nextLevelDoor)is.readObject();
                is.readObject();
                watcherGame.gameOver = (Boolean )is.readObject();
                watcherGame.repaint();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    });

    // Thread to get game as player
    Thread getGame = new Thread (()->{
        try{
            // to get data needed to make the game at first
            int width = (Integer) is.readObject();
            int height = (Integer) is.readObject();
            int enemy = (Integer) is.readObject();
            Rectangle[][] rectangles = (Rectangle[][]) is.readObject();
            ArrayList<ClientEnemy> enemies = (ArrayList<ClientEnemy>) is.readObject();
            nextLevelDoor thisDoor = (nextLevelDoor) is.readObject();
            game = new ClientGameFrame(width , height , enemy , rectangles ,
                    enemies , thisDoor , username );
            game.gamePanel.players = (ArrayList<BomberMan>)is.readObject();
            game.setVisible(true);

        } catch (IOException e) {
            e.printStackTrace();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        // to get game data from server
        while(!socket.isClosed()&& !game.gamePanel.gameOver){
            try{
                game.gamePanel.rectangles = (Rectangle[][]) is.readObject();
                game.gamePanel.enemies = (ArrayList<ClientEnemy>)is.readObject();
                BomberMan b = (BomberMan)is.readObject();
                game.gamePanel.myBomberMan.pixelI = b.pixelI;
                game.gamePanel.myBomberMan.pixelJ = b.pixelJ;
                game.gamePanel.myBomberMan.speed = b.speed;
                game.gamePanel.myBomberMan.bombControling = b.bombControling;
                game.gamePanel.myBomberMan.bombLimit = b.bombLimit;
                game.gamePanel.myBomberMan.bombRadius = b.bombRadius;
                game.gamePanel.myBomberMan.score = b.score;
                game.gamePanel.myBomberMan.ghostMode = b.ghostMode;
                game.gamePanel.myBomberMan.isAlive = b.isAlive;
                game.gamePanel.players = (ArrayList<BomberMan>)is.readObject();
                game.gamePanel.thisDoor = (nextLevelDoor)is.readObject();
                game.gamePanel.level = (Integer)is.readObject();

                // read if the game is over
                game.gamePanel.gameOver = (Boolean)is.readObject();
                game.repaint();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    });
    // Thread to request my bomber man action to server
    Thread requestAction = new Thread(()->{

        // to send my action to server
        while(!socket.isClosed()){

            // sending bomber man move if it is requested
            try{
                if(game != null && game.gamePanel.keyEvent != null ) {
                    os.writeObject(game.gamePanel.keyEvent.getKeyCode());
                    game.gamePanel.keyEvent = null ;
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    });
}
