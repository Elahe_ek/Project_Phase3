import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class ClientGamePanel extends JPanel implements KeyListener,ActionListener {
    int width ;
    int height ;
    Rectangle[][] rectangles ;
    ArrayList<ClientEnemy> enemies = new ArrayList<>();
    ArrayList<BomberMan> players = new ArrayList<>();
    nextLevelDoor thisDoor;
    ClientBomberMan myBomberMan;
    int level;
    int translateX;
    int translateY;
    KeyEvent keyEvent;
    Boolean gameOver;

    // the constructor
    public ClientGamePanel(int w , int h , Rectangle[][] r , ArrayList<ClientEnemy> e , nextLevelDoor d ) {
        width = w;
        height = h;
        rectangles = r ;
        enemies = e;
        thisDoor = d;
        myBomberMan = new ClientBomberMan();
        level = 1;
        translateX =0;
        translateY = 0;
        gameOver = false;
        addKeyListener(this);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

            if(myBomberMan.pixelI - translateX > 14*50 && myBomberMan.pixelI < width*50 - 15*50 +20)
                translateX += myBomberMan.ghadam ;
            if(myBomberMan.pixelI - translateX < 14*50 && myBomberMan.pixelI > 14 * 50 )
                translateX -= myBomberMan.ghadam ;

            if(myBomberMan.pixelJ - translateY > 7*50 && myBomberMan.pixelJ < height*50 - 8*50 +25 )
                translateY += myBomberMan.ghadam ;
            if(myBomberMan.pixelJ - translateY < 7*50 && myBomberMan.pixelJ > 7 * 50 )
                translateY -= myBomberMan.ghadam ;

            for (int k = 0; k < width; k++) {
                for (int l = 0; l < height; l++) {
                    g2.drawImage(rectangles[k][l].grassImage.getImage(), k * 50- translateX, l * 50 - translateY, null);

                    g.drawImage(rectangles[k][l].image.getImage(), k * 50 - translateX, l * 50 - translateY, null);
                    if(rectangles[k][l] instanceof Grass){
                        if(((Grass)rectangles[k][l]).bomb!= null){
                            g.drawImage(((Grass)rectangles[k][l]).bomb.image.getImage(), k * 50- translateX, l * 50 - translateY, null);
                        }
                        if(((Grass)rectangles[k][l]).bonus!= null && ((Grass)rectangles[k][l]).bonus.released ){
                            g.drawImage(((Grass)rectangles[k][l]).bonus.image.getImage(), k * 50 +5 - translateX, l * 50+5-translateY , null);
                        }

                    }

                }
            }

            for (int i = 0; i <enemies.size() ; i++) {
                if(enemies.get(i)!= null) {
                    g2.drawImage(enemies.get(i).image.getImage(), enemies.get(i).pixelI - translateX, enemies.get(i).pixelJ - translateY, null);
                }


            }

            if(thisDoor!= null && thisDoor.found)
                g2.drawImage(thisDoor.image.getImage() , thisDoor.i*50 - translateX, thisDoor.j*50 - translateY , null);

        for (int i = 0; i < players.size(); i++) {
            if(players.get(i).isAlive || players.get(i).showDeadFace )
                g2.drawImage(players.get(i).image.getImage(), players.get(i).pixelI - translateX, players.get(i).pixelJ - translateY , null);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(myBomberMan.isAlive)
            keyEvent = e ;
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
