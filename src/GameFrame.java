import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class GameFrame extends JFrame implements Serializable {
    // panels in server game frame
    JPanel dataPanel;
    JPanel playerPanel;
    RectanglesPanel rectanglespanel;
    ArrayList<BomberMan> players ;
    JLabel timeLabel;
    int width ;
    int height ;
    int enemy ;
    long lastGameTime ;
    boolean loading ;
    String name;
    int max ;
    Boolean gameOver ;

    GameFrame(int w , int h  , int enemies , String n , int m) {
        loading = false;
        width = w ;
        height = h ;
        enemy = enemies ;
        name = n;
        max = m;
        lastGameTime = 0;
        gameOver = false;
        setTitle("BomberMan");
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        players = new ArrayList<>();
        rectanglespanel = new RectanglesPanel(w , h , this  , 1 , enemy);

        dataPanel = new JPanel();
        dataPanel.setPreferredSize(new Dimension(w*50, 25));
        dataPanel.setMinimumSize(new Dimension(w*50, 25));
        dataPanel.setMaximumSize(new Dimension(w*50, 25));
        dataPanel.setBackground(Color.lightGray);
        dataPanel.setLayout(new BoxLayout(dataPanel , BoxLayout.X_AXIS));

        JLabel levelLabel = new JLabel("Level "+rectanglespanel.level);
        levelLabel.setSize(w*10 , 20);
        levelLabel.setMinimumSize(new Dimension(100 , 20));
        levelLabel.setMaximumSize(new Dimension(100 , 20));
        dataPanel.add(levelLabel);

        timeLabel = new JLabel("Time : ");
        timeLabel.setSize(w*10 , 20);
        timeLabel.setMinimumSize(new Dimension(100 , 20));
        timeLabel.setMaximumSize(new Dimension(100 , 20));
        dataPanel.add(timeLabel);

        JLabel scoreLabel = new JLabel("Score : ");
        scoreLabel.setSize(w*10 , 20);
        scoreLabel.setMinimumSize(new Dimension(100 , 20));
        scoreLabel.setMaximumSize(new Dimension(100 , 20));
        dataPanel.add(scoreLabel);
        add(dataPanel);

        playerPanel = new JPanel();
        playerPanel.setPreferredSize(new Dimension(w*50, 25));
        playerPanel.setMinimumSize(new Dimension(w*50, 25));
        playerPanel.setMaximumSize(new Dimension(w*50, 25));
        playerPanel.setLayout(new BoxLayout(playerPanel , BoxLayout.X_AXIS));

        JLabel speedLabel = new JLabel("Speed : 2 blocks per s");
        speedLabel.setSize(w*12 , 20);
        speedLabel.setMinimumSize(new Dimension(120 , 20));
        speedLabel.setMaximumSize(new Dimension(120 , 20));
        playerPanel.add(speedLabel);

        JLabel bombLimitLabel = new JLabel("BombLimit : 1");
        bombLimitLabel.setSize(w*10 , 20);
        bombLimitLabel.setMinimumSize(new Dimension(100 , 20));
        bombLimitLabel.setMaximumSize(new Dimension(100 , 20));
        playerPanel.add(bombLimitLabel);

        JLabel bombRadiusLabel = new JLabel("Radius : ");
        bombRadiusLabel.setSize(w*8 , 20);
        bombRadiusLabel.setMinimumSize(new Dimension(80 , 20));
        bombRadiusLabel.setMaximumSize(new Dimension(80 , 20));
        playerPanel.add(bombRadiusLabel);

        JLabel ghost = new JLabel("Mode");
        ghost.setIcon(new ImageIcon(new File("").getAbsolutePath() + "/images/me.png"));
        ghost.setMinimumSize(new Dimension(80 , 20));
        ghost.setMaximumSize(new Dimension(80 , 20));
        playerPanel.add(ghost);

        JLabel control = new JLabel();
        control.setSize(w*4 , 20);
        control.setMinimumSize(new Dimension(40 , 20));
        control.setMaximumSize(new Dimension(40 , 20));
        playerPanel.add(control);
        add(playerPanel);

        add(rectanglespanel);
        pack();
        setLocationRelativeTo(null);
        setSize(w*50 , (h*50)+70);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public void addPlayer(BomberMan b ){
        players.add(b);
    }

}
