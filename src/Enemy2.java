import javax.swing.*;
import java.io.File;
import java.io.Serializable;

public class Enemy2 extends Enemy implements Serializable{

    public Enemy2(int location_i, int location_j) {
        super(location_i, location_j , 2);
        speed = 1;
        this.image = new ImageIcon(new File("").getAbsolutePath() + "/images/monster-2.png");
    }

    @Override
    void enemyMove(RectanglesPanel r) {

        chasingMove(r);
    }
}
