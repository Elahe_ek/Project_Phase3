// A Java program for a Server
import java.awt.event.KeyEvent;
import java.net.*;
import java.io.*;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class Server {

    // the socket used by server
    private ServerSocket    serverSocket   = null;
    // a unique ID for each connection
    private static int uniqueId;
    // the boolean that will be turned of to stop the server
    Boolean keepGoing ;
    // the port number to listen for connection
    int port ;
    // an ArrayList to keep the list of the Client
    ArrayList<ClientThread> clients ;
    // an ArrayList to keep the list of the Games
    ArrayList<GameFrame> games ;
    // server Frame
    ServerFrame serverFrame ;

    public Server(int p , ServerFrame s) {
        port = p;
        serverFrame = s;
        // ArrayList for Clients
        clients = new ArrayList<>();
        // ArrayList for Games
        games = new ArrayList<>();
        // display on the server frame that we have started
        serverFrame.appendEvent("Server started");
        // the server failed
        // ?
    }

    public void startServer() {
        keepGoing = true;
		/* create socket server and wait for connection requests */
        try
        {
            // the socket used by the server
            serverSocket = new ServerSocket(port);

            // infinite loop to wait for connections
            while(keepGoing)
            {
                // display on the server frame that we are waiting
                serverFrame.appendEvent("Server waiting for clients on port " + port);

                Socket socket = serverSocket.accept();  	// accept connection
                // if I was asked to stop
                if(!keepGoing)
                    break;
                ClientThread t = new ClientThread(socket);  // make a thread of it
                clients.add(t);									// save it in the ArrayList
                t.start();
            }
            // I was asked to stop
            try {
                serverSocket.close();
                for(int i = 0; i < clients.size(); ++i) {
                    ClientThread tc = clients.get(i);
                    try {
                        tc.ois.close();
                        tc.oos.close();
                        tc.socket.close();
                    }
                    catch(IOException ioE) {
                        // not much I can do
                    }
                }
            }
            catch(Exception e) {
                System.out.println("Exception closing the server and clients: " + e);
            }
        }
        // something went bad
        catch (IOException e) {
            System.out.println(" Exception on new ServerSocket: " + e );
        }
    }

    // to remove the game from the list when it's over
    Thread removeGameOver = new Thread(()->{
        // search in all games for a game over
        for (int i = 0; i < games.size() ; i++) {
            if(games.get(i).gameOver) {
                games.remove(i);
            }

        }

    });

    /*
     * For the Frame to stop the server
     */
    protected void stop() {
        keepGoing = false;
        // connect to myself as Client to exit statement
        // Socket socket = serverSocket.accept();
        try {
            new Socket("localhost", port);
        }
        catch(Exception e) {
            // nothing I can really do
        }
    }

    class ClientThread extends Thread{

        // the socket where to listen/talk
        Socket socket;
        // both Data Stream
        ObjectInputStream ois;
        ObjectOutputStream oos;
        // game of this client
        private GameFrame game ;
        // my unique id (easier for deconnection)
        int id;
        // to send game data each 100 ms
        long lastSent;
        // this client's username
        String username;
        // number of this client in the player list
        int number ;

        // Constructor
        ClientThread(Socket socket) {
            // a unique id
            id = ++uniqueId;
            // time of last data sending
            lastSent = 0;
            this.socket = socket;
			/* Creating both Data Stream */
            try
            {
                // create output first
                oos = new ObjectOutputStream(socket.getOutputStream());
                ois  = new ObjectInputStream(socket.getInputStream());

            }
            catch (IOException e) {
                serverFrame.appendEvent("Exception creating new Input/output Streams: " + e );
                return;
            }

            // get the username and display it on the event panel
            try {
                username = (String) ois.readObject();
                serverFrame.appendEvent(username + " Just Connected.");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        // to send game data to client permanently when connected
        Thread sendGame = new Thread (()->{
            // sending data to client to create its game frame for the first time
            try{
                ArrayList<ClientEnemy> e = new ArrayList<>();
                for (int i = 0; i < game.rectanglespanel.enemies.size(); i++) {
                    e.add(new ClientEnemy(game.rectanglespanel.enemies.get(i).image , game.rectanglespanel.enemies.get(i).pixelI, game.rectanglespanel.enemies.get(i).pixelJ));
                }
                oos.writeObject(game.width);
                oos.writeObject(game.height);
                oos.writeObject(game.enemy);
                oos.writeObject(game.rectanglespanel.rectangles);
                oos.writeObject(e);
                oos.writeObject(game.rectanglespanel.thisDoor);
                oos.writeObject(game.players);
                game.players.get(number).zero = System.currentTimeMillis();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Boolean gameOverSent = false ;

            // sending data during the game to client
            while(!socket.isClosed() && !gameOverSent){
                if(System.currentTimeMillis()- lastSent >100){
                    try{
                        oos.reset();
                        oos.writeObject(game.rectanglespanel.rectangles);
                        // creating an ArrayList for Client enemies based on server's game enemies info and writing it to client
                        ArrayList<ClientEnemy> e = new ArrayList<>();
                        for (int i = 0; i < game.rectanglespanel.enemies.size(); i++) {
                            e.add(new ClientEnemy(game.rectanglespanel.enemies.get(i).image , game.rectanglespanel.enemies.get(i).pixelI, game.rectanglespanel.enemies.get(i).pixelJ));
                        }
                        oos.writeObject(e);
                        oos.writeObject(game.players.get(number));
                        oos.writeObject(game.players);
                        oos.writeObject(game.rectanglespanel.thisDoor);
                        oos.writeObject(game.rectanglespanel.level);
                        boolean gameover = game.gameOver;
                        if(game.gameOver)
                            removeGameOver.start();
                        if(gameover) {
                            gameOverSent = true;
                        }
                        oos.writeObject(game.gameOver);
                        lastSent = System.currentTimeMillis();
                    } catch (IOException e1) {
                        System.out.println("Exception writing to client: " + e1);
                    }
                }
            }
        });

        // to send game data to watcher client permanently when connected
        Thread sendToWatch = new Thread (()->{
            // sending data to watcher client to create its game frame for the first time
            try{
                ArrayList<ClientEnemy> e = new ArrayList<>();
                for (int i = 0; i < game.rectanglespanel.enemies.size(); i++) {
                    e.add(new ClientEnemy(game.rectanglespanel.enemies.get(i).image , game.rectanglespanel.enemies.get(i).pixelI, game.rectanglespanel.enemies.get(i).pixelJ));
                }
                oos.writeObject(game.width);
                oos.writeObject(game.height);
                oos.writeObject(game.rectanglespanel.rectangles);
                oos.writeObject(e);
                oos.writeObject(game.rectanglespanel.thisDoor);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Boolean gameOverSent = false ;
            // sending data during the game to watcher client
            while(!socket.isClosed() && !gameOverSent){
                if(System.currentTimeMillis()- lastSent >100){
                    try{
                        oos.reset();
                        oos.writeObject(game.rectanglespanel.rectangles);
                        ArrayList<ClientEnemy> e = new ArrayList<>();
                        for (int i = 0; i < game.rectanglespanel.enemies.size(); i++) {
                            e.add(new ClientEnemy(game.rectanglespanel.enemies.get(i).image , game.rectanglespanel.enemies.get(i).pixelI, game.rectanglespanel.enemies.get(i).pixelJ));
                        }
                        oos.writeObject(e);
                        oos.writeObject(game.players);
                        oos.writeObject(game.rectanglespanel.thisDoor);
                        oos.writeObject(game.rectanglespanel.level);
                        boolean gameover = game.gameOver;
                        if(gameover)
                            gameOverSent = true;
                        oos.writeObject(game.gameOver);
                        lastSent = System.currentTimeMillis();
                    } catch (IOException e1) {
                        System.out.println("Exception writing to client: " + e1);
                    }
                }
            }
        });

        // to get the action client requests
        Thread getClientAction= new Thread (()->{
            while(!socket.isClosed()){
                try{
                    int e = (Integer) ois.readObject();
                    // moving events
                    if (e == KeyEvent.VK_RIGHT && game.players.get(number).isAlive) {
                        game.players.get(number).move=1;
                    }
                    if (e== KeyEvent.VK_UP && game.players.get(number).isAlive) {
                        game.players.get(number).move=2;
                    }
                    if (e== KeyEvent.VK_LEFT && game.players.get(number).isAlive) {
                        game.players.get(number).move = 3;
                    }
                    if (e== KeyEvent.VK_DOWN && game.players.get(number).isAlive) {
                        game.players.get(number).move =4;
                    }
                    // bomb event
                    if (e== KeyEvent.VK_SPACE) {
                        if(game.players.get(number).bombLimit> game.rectanglespanel.bombs.size() && game.players.get(number).isAlive) {
                            // making bomb in the bomber man location and starting the threads
                            Bomb this_bomb = new Bomb(game.players.get(number).location_i,game.players.get(number).location_j);
                            ((Grass) game.rectanglespanel.rectangles[game.players.get(number).location_i][game.players.get(number).location_j]).bomb = this_bomb;
                            game.rectanglespanel.bombs.add(this_bomb);
                            game.rectanglespanel.rectangles[game.players.get(number).location_i][game.players.get(number).location_j].canCross = false;
                            game.rectanglespanel.rectangles[game.players.get(number).location_i][game.players.get(number).location_j].canCrossBomberman = false;
                            this_bomb.explode(game.players.get(number));
                            this_bomb.remove(game.rectanglespanel , game.players.get(number));
                        }
                    }
                    // to explode oldest bomb on the field when client has bomb controller and requests it
                    if (e== KeyEvent.VK_ENTER && game.players.get(number).isAlive && game.players.get(number).bombControling) {
                        try {
                            game.rectanglespanel.bombs.get(0).quickExplode(game.players.get(number));
                            game.rectanglespanel.bombs.get(0).quickRemove(game.rectanglespanel, game.players.get(number));
                        }catch (Exception ex){

                        }
                    }
                    game.rectanglespanel.repaint();
                } catch (IOException e1) {
                    System.out.println("Exception reading from client: " + e1);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        public void run(){
            try {
                // read the menu to start the game
                Menu menu = (Menu) ois.readObject();
                // read what button on the menu client has chosen
                String work = (String)ois.readObject();

                if(work.equals("start")) {
                    serverFrame.appendEvent(username + " Just started a solo game ");
                    if (!menu.widthText.getText().equals("") && !menu.heightText.getText().equals("")) {
                        int userWidth = parseInt(menu.widthText.getText().trim());
                        int userHeight = parseInt(menu.heightText.getText().trim());
                        int enemyNumber;
                        try {
                            enemyNumber = parseInt(menu.enemyText.getText().trim());
                        } catch (java.lang.NumberFormatException ne) {
                            enemyNumber = -1;
                        }

                        // to get name of the game if entered
                        String name ;
                        if(!menu.nameText.getText().trim().equals("")) {
                            name = menu.nameText.getText().trim();
                        }
                        else {
                            name = username + "'s game";
                        }
                        // to get max number of players if entered, else make it a solo
                        int max ;
                        if(!menu.numberText.getText().trim().equals("")) {
                            max = parseInt(menu.numberText.getText().trim());
                        }
                        else {
                            max = 1;
                        }
                        game = new GameFrame(userWidth, userHeight, enemyNumber , name , max);
                        number = game.players.size();
                        game.addPlayer(new BomberMan());
                        game.players.get(number).moveAndLevelUp(game.rectanglespanel);
                        game.players.get(number).setScoreControl();
                        games.add(game);
                        sendGame.start();
                        getClientAction.start();
                        game.rectanglespanel.checkGameOver();
                        game.setVisible(false);
                    }
                }

                if(work.equals("list")){
                    // send games name to client
                    ArrayList gameNames = new ArrayList();
                    for (int i = 0; i <games.size() ; i++) {
                        gameNames.add(games.get(i).name);
                    }
                    oos.writeObject(gameNames);

                    while(true){
                        // read the number of the game and a player/watcher boolean requested
                        int num = (Integer) ois.readObject();
                        Boolean player = (Boolean) ois.readObject();
                        // check if the client can join that game
                        if(player) {
                            if (games.get(num).players.size() < games.get(num).max) {
                                // if yes , tell the client and then start sending game data
                                oos.writeObject(true);
                                serverFrame.appendEvent(username + " Just joined game: " + games.get(num).name);
                                game = games.get(num);
                                BomberMan b = new BomberMan();
                                number = game.players.size();
                                game.addPlayer(b);
                                game.players.get(number).moveAndLevelUp(game.rectanglespanel);
                                game.players.get(number).setScoreControl();
                                sendGame.start();
                                getClientAction.start();
                                game.rectanglespanel.checkGameOver();
                                return;
                            }
                            // if no , inform the client to choose another game
                            oos.writeObject(false);
                        }
                        else {
                            // as watcher mode is always available , set the client game and start sending game data
                            game = games.get(number);
                            sendToWatch.start();
                        }
                    }
                }
            }
            catch(IOException i)
            {
                System.out.println(i);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
