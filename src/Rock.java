import javax.swing.*;
import java.io.File;
import java.io.Serializable;

public class Rock extends Rectangle implements Serializable{

    Rock(int x, int y, int width, int height) {
        super( x, y, width, height, false , new ImageIcon(new File("").getAbsolutePath()+"/images/rock.png"));
    }
}
