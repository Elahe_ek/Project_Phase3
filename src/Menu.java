import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class Menu extends JFrame {

    // to insert information to start new game
    JLabel widthLabel ;
    JLabel heightLabel;
    JLabel enemyLabel;
    JLabel nameLabel ;
    JLabel numberLabel;
    JTextField widthText;
    JTextField heightText;
    JTextField enemyText;
    JTextField nameText;
    JTextField numberText;
    // buttons to join other games or start a new one
    JButton start;
    JButton list ;
    // panel to show game lists if needed
    JPanel listPanel;
    // check boxes panel
    JPanel check ;
    // watcher/ player check box
    ButtonGroup group;
    JRadioButton watcher ;
    JRadioButton player ;
    // to hold game options when needed
    JScrollPane jsp;
    ArrayList<JButton> gameButtons;

    Menu() {

        super("Bomber Man");
        JLabel background = new JLabel(new ImageIcon(new File("").getAbsolutePath()+"/images/y.jpg"));
        this.setContentPane(background);
        this.setLocation(400 , 150);
        getContentPane().setLayout(null);

        gameButtons = new ArrayList<JButton>();

        widthLabel = new JLabel("WIDTH :");
        widthLabel.setLocation(370 , 240);
        widthLabel.setSize(80 , 20);
        add(widthLabel);

        heightLabel = new JLabel("HEIGHT :");
        heightLabel.setLocation(370 , 265);
        heightLabel.setSize(80 , 20);
        add(heightLabel);

        nameLabel = new JLabel("NAME :");
        nameLabel.setLocation(70 , 250);
        nameLabel.setSize(80 , 20);
        add(nameLabel);

        enemyLabel = new JLabel("ENEMIES :");
        enemyLabel.setLocation(370 , 290);
        enemyLabel.setSize(80 , 20);
        add(enemyLabel);

        numberLabel = new JLabel("MAX PLAYERS :");
        numberLabel.setLocation(70 , 290);
        numberLabel.setSize(120 , 20);
        add(numberLabel);

        widthText = new JTextField();
        widthText.setLocation(440 , 240);
        widthText.setSize(60 , 20);
        widthText.setFont(new Font("Arial", Font.PLAIN, 20));
        add(widthText);

        heightText = new JTextField();
        heightText.setLocation(440 , 265);
        heightText.setSize(60 , 20);
        heightText.setFont(new Font("Arial", Font.PLAIN, 20));
        add(heightText);

        nameText = new JTextField();
        nameText.setLocation(140 , 250);
        nameText.setSize(60 , 20);
        nameText.setFont(new Font("Arial", Font.PLAIN, 10));
        add(nameText);

        enemyText = new JTextField();
        enemyText.setLocation(440 , 290);
        enemyText.setSize(60 , 20);
        enemyText.setFont(new Font("Arial", Font.PLAIN, 20));
        add(enemyText);

        numberText = new JTextField();
        numberText.setLocation(180 , 290);
        numberText.setSize(60 , 20);
        numberText.setFont(new Font("Arial", Font.PLAIN, 10));
        add(numberText);

        start = new JButton();
        start.setText("NEW GAME");
        start.setLocation(220 , 80);
        start.setSize(150 , 50);
        start.setFont(new Font("Arial", Font.BOLD, 15));
        add(start);

        list = new JButton();
        list.setText("GAMES LIST");
        list.setLocation(220 , 140);
        list.setSize(new Dimension(150 , 50));
        list.setFont(new Font("Arial", Font.BOLD, 15));
        add(list);

        group = new ButtonGroup();

        player = new JRadioButton("player");
        watcher = new JRadioButton("watcher");
        player.setSelected(true);
        group.add(player);
        group.add(watcher);

        check = new JPanel();
        check.setLayout(new GridLayout(1 , 2));
        check.add(player);
        check.add(watcher);

        listPanel = new JPanel();
        listPanel.setLayout(new BoxLayout(listPanel , BoxLayout.Y_AXIS));
        listPanel.add(check);
        jsp = new JScrollPane(listPanel ,  ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jsp.setLocation(0 , 330);
        jsp.setSize(600 , 120);
        add(jsp);
    }
}
