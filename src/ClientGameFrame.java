import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class ClientGameFrame extends JFrame {
    ClientGamePanel gamePanel;
    JPanel dataPanel;
    JPanel playerPanel;
    JLabel timeLabel;
    JLabel levelLabel;
    JLabel speedLabel;

    JLabel bombRadiusLabel;
    JLabel bombLimitLabel;
    JLabel ghost;
    JLabel control;

    Thread ghostCheck;
    Thread controlCheck;
    Thread bombLimit;
    Thread bombRadius;
    Thread score;
    Thread time;
    Thread speed;
    Thread level;

    int width ;
    int height ;
    int enemy ;

    public ClientGameFrame(int width, int height, int enemy , Rectangle[][] rectangles, ArrayList<ClientEnemy> enemies , nextLevelDoor door , String s)  {
        this.width = width;
        this.height = height;
        this.enemy = enemy;
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        setTitle(s+"'s game frame");

        gamePanel = new ClientGamePanel(width , height ,rectangles , enemies , door );
        gamePanel.setFocusable(true);
        gamePanel.requestFocusInWindow();

        dataPanel = new JPanel();
        dataPanel.setPreferredSize(new Dimension(width*50, 25));
        dataPanel.setMinimumSize(new Dimension(width*50, 25));
        dataPanel.setMaximumSize(new Dimension(width*50, 25));
        dataPanel.setBackground(Color.lightGray);
        dataPanel.setLayout(new BoxLayout(dataPanel , 0));
        // level label
        levelLabel = new JLabel("Level "+ gamePanel.level);
        levelLabel.setSize(width*10 , 20);
        levelLabel.setMinimumSize(new Dimension(100 , 20));
        levelLabel.setMaximumSize(new Dimension(100 , 20));
        level = new Thread(()->{
            while(!gamePanel.gameOver){
                levelLabel.setText("Level "+gamePanel.level);
            }
        });
        level.start();
        dataPanel.add(levelLabel);

        // time label
        timeLabel = new JLabel("Time : ");
        timeLabel.setSize(width*10 , 20);
        timeLabel.setMinimumSize(new Dimension(100 , 20));
        timeLabel.setMaximumSize(new Dimension(100 , 20));
        time = new Thread(()->{
            while (!gamePanel.gameOver) {
                long t = System.currentTimeMillis()- gamePanel.myBomberMan.zero ;
                long minutes = t / 60000 ;
                long seconds = (t - ((t / 60000) * 60000)) / 1000 ;
                String secondsString = "" +seconds;
                if(seconds<10)
                    secondsString = "0"+seconds;

                timeLabel.setText("Time : " + minutes + ":" + secondsString);
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        time.start();
        dataPanel.add(timeLabel);
        // score label
        JLabel scoreLabel = new JLabel("Score : ");
        scoreLabel.setSize(width*10 , 20);
        scoreLabel.setMinimumSize(new Dimension(100 , 20));
        scoreLabel.setMaximumSize(new Dimension(100 , 20));
        score = new Thread(()->{
            while(true) {
                scoreLabel.setText("Score : " + gamePanel.myBomberMan.score);
            }
        });
        score.start();
        dataPanel.add(scoreLabel);
        add(dataPanel);
        // player panel
        playerPanel = new JPanel();
        playerPanel.setPreferredSize(new Dimension(width*50, 25));
        playerPanel.setMinimumSize(new Dimension(width*50, 25));
        playerPanel.setMaximumSize(new Dimension(width*50, 25));
        playerPanel.setLayout(new BoxLayout(playerPanel , 0));
        //speed label
        speedLabel = new JLabel("Speed : 2 blocks per s");
        speedLabel.setSize(width*12 , 20);
        speedLabel.setMinimumSize(new Dimension(120 , 20));
        speedLabel.setMaximumSize(new Dimension(120 , 20));
        speed = new Thread(()->{
            while(true) {
                speedLabel.setText("Speed : " + gamePanel.myBomberMan.speed +" block/s");
            }
        });
        speed.start();
        playerPanel.add(speedLabel);
        //bomb limit label
        bombLimitLabel = new JLabel("BombLimit : 1");
        bombLimitLabel.setSize(width*10 , 20);
        bombLimitLabel.setMinimumSize(new Dimension(100 , 20));
        bombLimitLabel.setMaximumSize(new Dimension(100 , 20));
        bombLimit = new Thread(()->{
            while(true) {
                bombLimitLabel.setText("bomb limit : " + gamePanel.myBomberMan.bombLimit);
            }
        });
        bombLimit.start();
        playerPanel.add(bombLimitLabel);
        // bomb radius label
        bombRadiusLabel = new JLabel("Radius : ");
        bombRadiusLabel.setSize(width*8 , 20);
        bombRadiusLabel.setMinimumSize(new Dimension(80 , 20));
        bombRadiusLabel.setMaximumSize(new Dimension(80 , 20));
        bombRadius = new Thread(()->{
            while(true) {
                bombRadiusLabel.setText("Radius : " + gamePanel.myBomberMan.bombRadius);
            }
        });
        bombRadius.start();
        playerPanel.add(bombRadiusLabel);
        // ghost mode label
        ghost = new JLabel("Mode");
        ghost.setIcon(new ImageIcon(new File("").getAbsolutePath() + "/images/me.png"));
        ghost.setMinimumSize(new Dimension(80 , 20));
        ghost.setMaximumSize(new Dimension(80 , 20));
        ghostCheck = new Thread(()->{
            while(true) {
                if(gamePanel.myBomberMan.ghostMode) {

                    ghost.setIcon(new ImageIcon(new File("").getAbsolutePath() + "/images/spirit.png"));
                }
                else{
                    ghost.setIcon(new ImageIcon(new File("").getAbsolutePath() + "/images/me.png"));
                }
            }
        });
        ghostCheck.start();
        playerPanel.add(ghost);

        // bomb control bonus label
        control = new JLabel();
        control.setSize(width*4 , 20);
        control.setMinimumSize(new Dimension(40 , 20));
        control.setMaximumSize(new Dimension(40 , 20));
        controlCheck = new Thread(()->{
            while(true) {
                if(gamePanel.myBomberMan.bombControling) {
                    control.setIcon(new ImageIcon(new File("").getAbsolutePath() + "/images/control.png"));
                }
                else{
                    control.setText("");

                }
            }
        });
        controlCheck.start();
        playerPanel.add(control);
        add(playerPanel);

        add(gamePanel);
        pack();
        setLocationRelativeTo(null);
        setSize(width*50 , (height*50)+70);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

}
