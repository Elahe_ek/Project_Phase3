import javax.swing.*;
import java.io.File;
import java.io.Serializable;

public class Grass extends Rectangle implements Serializable{
    Bomb bomb;
    Bonus bonus ;

    Grass(int x, int y, int width, int height) {
        super(x, y, width, height , true);
        bonus = null;
        image = new ImageIcon(new File("").getAbsolutePath()+"/images/unknown.jpg");
    }
}
