import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;

class RectanglesPanel extends JPanel implements Serializable {
    private static final int PREF_W = 50;
    private static final int PREF_H = PREF_W;
    Rectangle[][] rectangles ;
    GameFrame gameFrame ;
    int w ;
    int h ;
    int level ;
    int enemyNum ;
    int translateX ;
    int translateY ;
    nextLevelDoor thisDoor;
    ArrayList<Integer>randomX ;
    ArrayList<Integer> randomY ;
    ArrayList<Bomb> bombs = new ArrayList<>();
    ArrayList<Enemy> enemies = new ArrayList<>();
    ArrayList<Wall> randomWalls ;
    ArrayList<Integer> wallsForBonus ;

    RectanglesPanel(int width, int height , GameFrame g , int l , int enemyInput) {
        this.w = width;
        this.h=height;
        this.gameFrame = g;
        this.level = l;
        translateX =0;
        translateY = 0;
        randomX = new ArrayList<>();
        randomY = new ArrayList<>();
        randomWalls = new ArrayList<>();
        wallsForBonus = new ArrayList<>();

        if(enemyInput == -1)
            enemyNum = Math.min(w/ 3, h/3);
        else
            enemyNum = enemyInput;

        setFocusable(true);
        makeRandomWalls();
        addRectangle();
        makeEnemies(level , enemyNum);
        makeRandomBonus();
        makeNextLevelDoor();
        for (int i = 0; i <enemies.size() ; i++) {
            enemies.get(i).enemyMove( this  );
        }
    }

    public void makeRandomWalls() {
        for (int i = 0; i <((w*h) /5) ; i++) {
            randomX.add(0) ;
            randomY.add(0);
            while(!canBeWall(randomX.get(i) , randomY.get(i))){
                randomX.set(i ,ThreadLocalRandom.current().nextInt(0, w) ) ;
                randomY.set(i , ThreadLocalRandom.current().nextInt(0, h)) ;
            }
        }
    }

    public boolean canBeWall(int thisX , int thisY) {
        if(thisX % 2 == 1 && thisY % 2 ==1)
            return false;

        return (!((thisX==0 || thisX==1)&&(thisY==0 || thisY==1))) ;
    }

    public boolean isRandomWall(int this_i, int this_j) {

        for (int i = 0; i < ((w*h)/5) ; i++) {
            if(randomX.size()>i && randomY.size()>i) {
                if (randomX.get(i) == this_i && randomY.get(i) == this_j)
                    return true;
            }
        }
        return false;
    }

    public void addRectangle() {
        rectangles = new Rectangle[w][h];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                boolean rock = false;
                Rectangle rect = null;
                if (i % 2 != 0 && j % 2 != 0) {
                    rect = new Rock(i * 50, j * 50, 50, 50);
                    rock = true;
                }
                if (isRandomWall(i, j)) {
                    rect = new Wall(i * 50, j * 50, 50, 50);
                    randomWalls.add((Wall) rect);
                }

                if(!rock && !isRandomWall(i , j)){
                    rect = new Grass(i * 50, j * 50, 50, 50);
                }
                rectangles[i][j] = rect;
            }
        }
    }

    private Enemy addEnemy(int t , int enemiesMade){
        if(t==1) {
            Enemy thisEnemy = new Enemy1(0, 0);
            enemies.add(thisEnemy);
            int i1 = 0;
            int j1 = 0;
            while (!canBeEnemy(i1, j1) || hasBeenMadeEnemy(i1, j1, enemiesMade)) {
                i1 = ThreadLocalRandom.current().nextInt(0, w);
                j1 = ThreadLocalRandom.current().nextInt(0, h);
            }

            return new Enemy1(i1, j1);
        }
        if(t==2) {
            Enemy thisEnemy = new Enemy2(0, 0);
            enemies.add(thisEnemy);
            int i1 = 0;
            int j1 = 0;
            while (!canBeEnemy(i1, j1) || hasBeenMadeEnemy(i1, j1, enemiesMade)) {
                i1 = ThreadLocalRandom.current().nextInt(0, w);
                j1 = ThreadLocalRandom.current().nextInt(0, h);
            }

            return new Enemy2(i1, j1);
        }
        if(t==3) {
            Enemy thisEnemy = new Enemy3(0, 0);
            enemies.add(thisEnemy);
            int i1 = 0;
            int j1 = 0;
            while (canBeEnemy(i1, j1) || hasBeenMadeEnemy(i1, j1, enemiesMade)) {
                i1 = ThreadLocalRandom.current().nextInt(0, w);
                j1 = ThreadLocalRandom.current().nextInt(0, h);
            }

            return new Enemy3(i1, j1);
        }
        if(t==4) {
            Enemy thisEnemy = new Enemy4(0, 0);
            enemies.add(thisEnemy);
            int i1 = 0;
            int j1 = 0;
            while (!canBeEnemy(i1, j1) || hasBeenMadeEnemy(i1, j1, enemiesMade)) {
                i1 = ThreadLocalRandom.current().nextInt(0, w);
                j1 = ThreadLocalRandom.current().nextInt(0, h);
            }

            return new Enemy4(i1, j1);
        }

        return null;

    }

    private void makeEnemies(int level , int num) {

        int enemiesMade = 0 ;

        if(level ==1 ) {
            for (int i = 0; i < num ; i++) {
                Enemy e = addEnemy(1 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }
        }
        if(level==2){
            for (int i = 0; i < num; i++) {
                int type = ThreadLocalRandom.current().nextInt(1, 3);
                if(type==1){
                    Enemy e = addEnemy(1 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }
                else {
                    Enemy e = addEnemy(2 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }
            }
        }

        if(level ==3 ){
            for (int i = 0; i < num; i++) {
                int type = ThreadLocalRandom.current().nextInt(1, 4);
                if(type==1){
                    Enemy e = addEnemy(1 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }
                if(type==2) {
                    Enemy e = addEnemy(2 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }
                if(type==3){
                    Enemy e = addEnemy(3 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }

            }
        }

        if(level ==4 ){
            ifLevelFourReached(num, enemiesMade);
        }

        if(level > 4 ){
            ifLevelFourReached((num*105)/100, enemiesMade);
        }

    }

    public void ifLevelFourReached(int number , int enemiesMade){

        for (int i = 0; i < number ; i++) {
            int type = ThreadLocalRandom.current().nextInt(1, 5);
            if(type==1){
                Enemy e = addEnemy(1 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }
            if(type==2) {
                Enemy e = addEnemy(2 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }
            if(type==3){
                Enemy e = addEnemy(3 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }
            if(type==4){
                Enemy e = addEnemy(4 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }

        }
    }

    private boolean canBeEnemy(int i , int j) {

        if(i % 2 == 1 && i % 2 ==1)
            return false;

        if(isRandomWall(i , j))
            return false;

        return (!((i==0 || i==1)&&(j==0 || j==1))) ;

    }

    private boolean hasBeenMadeEnemy(int i1 , int j1 , int enemiesMade){
        for (int j = 0; j < enemiesMade; j++) {
            if (enemies.get(j).location_i==i1 && enemies.get(j).location_j==j1){
                return  true;
            }

        }
        return false;
    }

    public void makeRandomBonus() {
        for (int i = 0; i < 2 * enemies.size(); i++) {
            int whichWall = 0;
            try {
                while (randomWalls.get(whichWall).bonus != null || hasBeenGivenBonus(whichWall)) {
                    whichWall = ThreadLocalRandom.current().nextInt(0, randomX.size());
                }
            }catch(IndexOutOfBoundsException e){

            }
            int goodorBAD = ThreadLocalRandom.current().nextInt(0, 2);
            boolean good = true;
            if (goodorBAD == 1)
                good = false;

            int whichType = ThreadLocalRandom.current().nextInt(1, 7);
            try{
                ((Wall) (rectangles[randomX.get(whichWall)][randomY.get(whichWall)])).bonus = new Bonus(good, whichType);
            }catch(ClassCastException cast){

            }
            wallsForBonus.add(whichWall);

        }
    }

    private boolean hasBeenGivenBonus(int whichWall) {
        for (int i = 0; i < wallsForBonus.size(); i++) {
            if(wallsForBonus.get(i)== whichWall)
                return true;
        }
        return false;
    }

    private void makeNextLevelDoor() {

        int door = ThreadLocalRandom.current().nextInt(0, randomX.size());
        try {
            while (((Wall) rectangles[randomX.get(door)][randomY.get(door)]).bonus != null) {
                door = ThreadLocalRandom.current().nextInt(0, randomX.size());
            }
        }
        catch (ClassCastException e){
            System.out.println(randomX.get(door));
            System.out.println(randomY.get(door));
            e.printStackTrace();
        }

        thisDoor = new nextLevelDoor(randomX.get(door) , randomY.get(door));
    }

    public Dimension getPreferredSize() {
        return new Dimension(PREF_W, PREF_H);
    }

    public void levelUp() {
        level ++ ;
        randomX = new ArrayList<>();
        randomY = new ArrayList<>();
        randomWalls = new ArrayList<>();
        makeRandomWalls();
        addRectangle();
        for (int i = 0; i <enemies.size() ; i++) {
            enemies.get(i).finished = true;
            enemies.remove(i);
        }
        for (int i = 0; i <wallsForBonus.size() ; i++) {
            wallsForBonus.remove(i);
        }
        makeEnemies(level , enemyNum);
        makeRandomBonus();
        makeNextLevelDoor();
        for (int i = 0; i <enemies.size() ; i++) {
            enemies.get(i).enemyMove( this  );
        }
        for (int i = 0; i <gameFrame.players.size() ; i++) {
            if(!gameFrame.players.get(i).isAlive){
                gameFrame.players.get(i).ghostMode = false;
                gameFrame.players.get(i).bombRadius = 2;
                gameFrame.players.get(i).bombControling = false;
                gameFrame.players.get(i).bombLimit = 2;
            }
            gameFrame.players.get(i).isAlive = true;
            gameFrame.players.get(i).image = new ImageIcon(new File("").getAbsolutePath()+"/images/superhero.png") ;
            gameFrame.players.get(i).pixelI = 5;
            gameFrame.players.get(i).pixelJ = 5;
            gameFrame.players.get(i).location_i = 0;
            gameFrame.players.get(i).location_j = 0;
            gameFrame.players.get(i).moveAndLevelUp(gameFrame.rectanglespanel);
            gameFrame.players.get(i).setScoreControl();

        }
    }

    public void checkGameOver(){
        Thread gameOver = new Thread(()->{
            while(true) {
                boolean over = true;
                for (int i = 0; i < gameFrame.players.size(); i++) {
                    if (gameFrame.players.get(i).isAlive) {
                        over = false;
                    }
                }
                if(over){
                    gameFrame.gameOver = true;
                    for (int i = 0; i <enemies.size() ; i++) {
                        enemies.get(i).finished = true;
                    }
                }
            }
        });
        gameOver.start();
    }

    public boolean allEnemiesDied() {
        for (int i = 0; i <enemies.size() ; i++) {
            try {
                if (!enemies.get(i).equals(null))
                    return false;
            }catch(NullPointerException ee){

            }
        }
        return true;
    }

    public void removeEnemy(int i , BomberMan b) {
        Thread killing = new Thread(() -> {

            try {
                sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            b.score+=enemies.get(i).level *20;
            enemies.remove(i);

        });
        killing.start();
    }

    public void removeWall(BomberMan bm , int i, int j) {

        Thread destroying = new Thread(() -> {

            try {
                sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            ((Wall)rectangles[i][j]).destroyed=true ;
            Bonus b = ((Wall)rectangles[i][j]).bonus ;
            rectangles[i][j]= new Grass(i , j , 50 , 50);
            ((Grass)rectangles[i][j]).bonus = b ;
            if(b!= null)
                b.released = true;
            bm.score+=10;

            if(thisDoor.i==i && thisDoor.j==j)
                thisDoor.found = true;

        });
        destroying.start();
    }

    public boolean noBomb(Rectangle r){
        if(!(r instanceof Grass))
            return true;
        else if(((Grass) r).bomb != null)
            return false ;
        return true;
    }

}
